<?php

namespace App\Http\Livewire;

use App\Models\Ingredients;
use Livewire\Component;

class CreateEtapes extends Component
{
    public $etapeSelected;
    public $ingredientSelected;

    public $ingredients;

    public function mount()
    {
        $this->etapeSelected = 1;
        $this->ingredientSelected = 0;
        $this->ingredients = Ingredients::all();
    }

    public function render()
    {
        return view('livewire.create-etapes');
    }
}
