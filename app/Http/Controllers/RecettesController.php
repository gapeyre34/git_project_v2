<?php

namespace App\Http\Controllers;

use App\Models\Difficulty;
use App\Models\Etapes;
use App\Models\Recettes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RecettesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        //
        $recettes = Recettes::all();
        $difficulties = Difficulty::all();
        $etapes = Etapes::all();


        return view('recettes.index', compact('recettes', 'difficulties', 'etapes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        //
        return view('recettes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        //On valide les input de la recette
        $request->validate([
            'recette_name'          => ['required'],['unique:recettes, recette_name'],
            'prep_timing'           => ['required'],
            'prep_com'              => ['required'],
            'difficulty_id'         => ['required'],
        ]);

        //On créé la recette et on récupère l'id
        $recette = $request->input();
        Recettes::create($recette);
        $recette_id = Recettes::where('recette_name', $recette['recette_name'])->get()->first()->id; // Pour la table de jointure

        //on compte le nombre de fois qu'on va devoir ajouter un ingredient
        $counter_ingredients = $request->input('ingredient_nbr');

        for ($i = 0; $i <= $counter_ingredients; $i++)
        {
            $request->validate([
               "ingredient_id".$i   => ['required'],
            ]);

            $ingredient_id = $request->input('ingredient_id'.$i);

            DB::insert('insert into ingredients_recettes (recettes_id, ingredients_id) values (?, ?)', [$recette_id, $ingredient_id]);
        }

        return redirect()->route('dashboard.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
